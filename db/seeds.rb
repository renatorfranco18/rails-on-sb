# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
User.create(
	[
		{
			name: 'Rails on SB'
			email: 'contato@railsonsb.org',
			password: '3223001Rf',
			password_confirmation: '3223001Rf',
			admin: true
		}
	]
)

admin = User.where(admin: true).first

Post.destroy_all
for i in 1..50
	Post.create({
		title: "A Post #{i}",
		content: Cicero.paragraphs(3),
		tags: '#rubyonrails #development #elixir #podcast',
		user: admin
	})
end