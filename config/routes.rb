# frozen_string_literal: true

Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
	# Authentication
	# @implemented
  devise_for :users, {}

  # Application
  # @implemented
  root to: 'home#index'

  # Routes
  # @implemented
  get 'sobre' 		=> 'home#about', 			as: :about
  get 'blog'  		=> 'posts#index', 		as: :blog
  get 'post/:id'	=> 'posts#show', 			as: :post
  get 'contribua' => 'home#contribute', as: :contribute
  get 'branding' 	=> 'home#branding',   as: :branding
  get 'vagas'			=> 'jobs#index',			as: :jobs
  get 'pesquisa'  => 'home#search', 		as: :search
end
