# frozen_string_literal: true

# == Schema Information
#
# Table name: posts
#
#  id          :integer          not null, primary key
#  content     :text
#  description :string
#  slug        :string
#  tags        :string
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#
# Indexes
#
#  index_posts_on_user_id  (user_id)
#
class Post < ApplicationRecord
	extend FriendlyId
	friendly_id :title, use: [:slugged]

	self.table_name  = 'posts'
	self.primary_key = 'id'

  belongs_to :user
end
