# frozen_string_literal: true

module Mailerable
	include ActiveSupport::Concern

	def mail_to(mailer, action, object = self, deliver_method = :deliver_now)
		logger = Rails.logger

		begin
			mailer.send(action.to_sym, object).send(deliver_method.to_sym)
		rescue Exception => e
			logger.warn e
			raise StandardError, e
		end
	end
end