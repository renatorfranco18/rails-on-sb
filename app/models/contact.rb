# frozen_string_literal: true

class Contact < Object
	include ActiveModel::Conversion
	include ActiveModel::Validations
	include Mailerable

	attr_accessor :name, :email, :message

	def initialize(params = {})
		@name    = params[:name]
		@email   = params[:email]
		@message = params[:message]

		@mailer  = ContactMailer
	end

	def persisted?
		false
	end

	def sendEmail
		mail_to(@mailer, :sended)
		mail_to(@mailer, :received)
	end

	validates :name,
						presence: true,
						uniqueness: false,
						allow_blank: false,
						length: { minimum: 3, maximum: 30 }

	validates :email,
						presence: true,
						uniqueness: false,
						allow_blank: false,
						length: { minimum: 3, maximum: 245 }

	validates :message,
						presence: true,
						uniqueness: false,
						allow_blank: false,
						length: { minimum: 3, maximum: 4_500 }
end