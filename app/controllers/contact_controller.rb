# frozen_string_literal: true

class ContactController < ApplicationController
	def index
		@contact = Contact.new
	end

	def create
		contact = Contact.new(contact_params)
		if contact.valid? && recaptcha_verify(model: contact)
			contact.sendEmail
			flash[:notice] = 'E-mail enviado com sucesso.'
			redirect_to root_path
		else
			render :index
		end
	end

	private

	def contact_params
		params.require(:contact).permit(:name, :email, :message)
	end
end