# frozen_string_literal: true

class HomeController < ApplicationController
	def index
		@posts = Post.order(created_at: :desc)
			.page(params[:page]).per(7)
	end

	def about
	end

	def contribute
	end

	def branding
	end

	def jobs
	end

	def search
	end
end